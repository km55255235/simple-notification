# Simple Notification



## Getting started

Pada tutorial ini terdapat 2 skema,
1. Trigger Notification Manually
2. Trigger Notification by FCM

### Trigger Notification Manually
Anda bisa lihat pada Main Activity

### Trigger Notification by FCM Push Notification
Skema yang sama, hanya yang membedakan cara triggernya.
Pada skema ini menggunakan Service yang di provide oleh FCM
Jangan lupa di daftarkan **Service** tersebut di Manifest
Kemudian, sebelum lebih lanjut anda bisa daftarkan project anda di console firebase, anda bisa manfaatkan tools **Firebase Assistant**
Pilihlah menu Cloud Messaging di Console tersebut dan anda tinggal kirim saja.
