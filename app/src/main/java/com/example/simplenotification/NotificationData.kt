package com.example.simplenotification

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NotificationData(
    var id: Int,
    var title: String? = null,
    var content: String? = null,
    var bigContent: String? = null
): Parcelable
