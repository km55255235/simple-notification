package com.example.simplenotification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FCMService: FirebaseMessagingService() {
    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationChannel: NotificationChannel
    private val CHANNEL_ID = "CHANNEL_NOTIFICATION_FCM"
    private val CHANNEL_DESCRIPTION = "CHANNEL_DESCRIPTION_NOTIFICATION_FCM"

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val title = remoteMessage.notification?.title
        val text = remoteMessage.notification?.body
        val notification = NotificationData(1, title, text)
        sendNotification(notification)
        super.onMessageReceived(remoteMessage);

    }

    private fun sendNotification(notification: NotificationData) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel =
                NotificationChannel(CHANNEL_ID, CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val bundle = Bundle()
        bundle.putParcelable("NOTIFICATION_ARGS", notification)
        val intent = Intent(this, AfterNotificationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtras(bundle)

        val pendingIntent = PendingIntent.getActivity(
            this, 101, intent,
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
        )

        val vibrate = longArrayOf(1000, 1000, 1000, 1000)

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notif)
            .setContentTitle(notification.title)
            .setContentText(notification.content)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(notification.bigContent)
            )
            .setVibrate(vibrate)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .setLights(Color.GREEN, 1, 1)
            .setWhen(System.currentTimeMillis())
            .setAutoCancel(true)

        notificationManager.notify(notification.id, builder.build())
    }
}