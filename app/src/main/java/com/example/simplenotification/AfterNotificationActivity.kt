package com.example.simplenotification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.simplenotification.databinding.ActivityAfterNotificationBinding

class AfterNotificationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAfterNotificationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAfterNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val data = intent.getParcelableExtra("NOTIFICATION_ARGS") as NotificationData?
        binding.textView.text = data?.title
    }
}